//
// Created by gihexdev00 on 2/25/23.
//

#include <widget/gihex_button.h>

#include <vector>

#include <GLES3/gl3.h>
#include <GLES3/gl3ext.h>
#include <GLES3/gl3platform.h>

#include <utils/log.h>
#include <utils/utils.h>

#include <graphics/gihex_shader.h>

namespace gihex {

    GihexButton::GihexButton(const GameActivity *activity, GxSize<u32> size_window, i32 density)
            : GihexWidget(
            size_window, density) {
        GLOGD("Create button");
        win_size = size_window;
        wgt_size = {80 * metrics_density, 50 * metrics_density};
        //create 1 vao
        glGenVertexArrays(1, &vao);
        //create 2 vbo for vertex position and vertex color
        glGenBuffers(2, &vbo[0]);

        //create shader program
//        std::string vert = AndroidUtils::get_asset_string("gihex_button.vert", "/shader",
//                                                          activity);
//        std::string frag = AndroidUtils::get_asset_string("gihex_button.frag", "/shader",
//                                                          activity);

        std::string vert = R"(#version 300 es
precision mediump float;
in vec2 vertex_position;
in vec4 colors;

out vec4 color;
void main(){
    gl_Position.xy=vertex_position;
    gl_Position.z=0.0;
    gl_Position.w=1.0;
    color=colors;
})";

        std::string frag = R"(#version 300 es
precision mediump float;

in vec4 color;
out vec4 fragColor;
void main(){
    fragColor=color;
//    fragColor=vec4(1.0,1.0,0.0,1.0);
})";
        G_ASSERT(!vert.empty())
        G_ASSERT(!frag.empty())
        shader_program = create_shader_program(vert, frag);
        if (!shader_program) {
            GLOGD("Failed create button shader program %u", shader_program);
            return;
        }
        glUseProgram(shader_program);

        f32 vx = x_gl_unit(get_position().x);
        f32 vy = y_gl_unit(get_position().y);
        f32 vw = x_gl_unit(get_position().x + wgt_size.w);
        f32 vh = y_gl_unit(get_position().y + wgt_size.h);

        vertex_position = {
                vx, vy,
                vx, vh,
                vw, vy,
                vw, vy,
                vx, vh,
                vw, vh
        };

        //set data to opengl es
        glBindVertexArray(vao);
        //position data
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * vertex_position.size(),
                     vertex_position.data(),
                     GL_STATIC_DRAW);
        i32 vertex_pos_attribute = glGetAttribLocation(shader_program, "vertex_position");
        glEnableVertexAttribArray(vertex_pos_attribute);
        glVertexAttribPointer(vertex_pos_attribute, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

//        color data
        glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * bg_color.size() * 4, bg_color.data(),
                     GL_DYNAMIC_DRAW);
        i32 color_attribute = glGetAttribLocation(shader_program, "colors");
        glEnableVertexAttribArray(color_attribute);
        glVertexAttribPointer(color_attribute, 4, GL_FLOAT, GL_FALSE, 0, nullptr);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    GihexButton::~GihexButton() {
        GLOGD("Destroy button...");
    }

    void GihexButton::key_event_handler(const GameActivityKeyEvent *event) {
        GihexWidget::key_event_handler(event);
        if (!is_visible || !is_enable) return;

    }

    void GihexButton::motion_event_handler(const GXEvent &event) {
        GihexWidget::motion_event_handler(event);
        if (!is_visible || !is_enable) return;
//        if (!event.is_on_screen) return;
        if (!is_on_area(widget_event.motion_x, widget_event.motion_y)) return;
//        if on area call:
        if (widget_event.type == GxEventType::GX_EVENT_TYPE_POINTER_UP) {
            if (!click) return;
            click();
        }
    }

    void GihexButton::update_widget() {
        GihexWidget::update_widget();
    }

    void GihexButton::render_widget() {
        GihexWidget::render_widget();
        if (!is_visible || !is_enable) return;
        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, vertex_position.size() / 2);
        glBindVertexArray(0);
    }

    void GihexButton::on_click(const std::function<void()> &click_handler) {
        click = click_handler;
    }

    bool GihexButton::is_on_area(f32 ptr_x, f32 ptr_y) const {
        f32 pad = 1.0f; //padding for pointer
//        bool on_area = ((ptr_x + pad - (f32) btn_pos.x) < ((f32) btn_size.w - pad)) &&
//                       ((ptr_y + pad - (f32) btn_pos.y) < ((f32) btn_size.h - pad));
//upgrade thizzz
        auto x = ((ptr_x - get_position().x) < wgt_size.w) &&
                 ((get_position().x - ptr_x) < 0);
        auto y = ((ptr_y - get_position().y) < wgt_size.h) &&
                 ((get_position().y - ptr_y) < 0);
//        GLOGD("on area: %d %d %d", x, y, (i32) ptr_x);
        return x && y;
    }

    void GihexButton::set_position(i32 x, i32 y) {
        GihexWidget::set_position(x, y);
        GLOGD("pos btn %f,%f", get_position().x, get_position().y);
        change_vertex_pos();
        //update draw
    }

    void GihexButton::set_color(Color color) {
        def_color = color;
        //update draw
    }

    void GihexButton::set_hover_color(Color color) {
        hover_color = color;
        //update draw
    }

    void GihexButton::set_visibility(bool visibility) {
        is_visible = visibility;
    }

    void GihexButton::enable_widget(bool enable) {
        is_enable = enable;
    }

    void GihexButton::change_vertex_pos() {
        vertex_position.clear();
        f32 vx = x_gl_unit(get_position().x);
        f32 vy = y_gl_unit(get_position().y);
        f32 vw = x_gl_unit(get_position().x + wgt_size.w);
        f32 vh = y_gl_unit(get_position().y + wgt_size.h);

        vertex_position = {
                vx, vy,
                vx, vh,
                vw, vy,
                vw, vy,
                vx, vh,
                vw, vh
        };
        glBindVertexArray(vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBufferData(GL_ARRAY_BUFFER, sizeof(f32) * vertex_position.size(),
                     vertex_position.data(),
                     GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    bool GihexButton::update_window_size_callback(GxSize<u32> new_win_size) {
        if (!GihexWidget::update_window_size_callback(new_win_size)) return false;
        change_vertex_pos();
        GLOGD("size win==> %dx%d", win_size.w, win_size.h);
        return true;
    }

} // gihex