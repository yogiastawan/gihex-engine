//
// Created by gihexdev00 on 2/24/23.
//

#include <widget/gihex_widget.h>
#include <layouts/gihex_layout.h>


namespace gihex {

    void GihexWidget::key_event_handler(const GameActivityKeyEvent *event) {

    }

    void GihexWidget::motion_event_handler(const GXEvent &event) {
        widget_event = event;
    }

    void GihexWidget::update_widget() {

    }

    void GihexWidget::render_widget() {

    }

    GihexWidget::GihexWidget(GxSize<u32> window_size, i32 density) {
        win_size = window_size;
        if (density < 160) {
            metrics_density = 1;
            return;
        }
        //fix add tv density
        metrics_density = (f32) density / 160.0f;
    }

    GihexWidget::~GihexWidget() {
//        delete util;
    }

    bool GihexWidget::update_window_size_callback(GxSize<u32> new_win_size) {
        if (!((win_size.w - new_win_size.w) + (win_size.h - new_win_size.h))) return false;
        win_size = new_win_size;
        return true;
    }

    void
    GihexWidget::set_position_relative(PositionRelative pos_rel) {

        // do not apply if pos contain more than 2 position
        if (pos_rel > 12) return;
        relative_pos = pos_rel;
        calculate_position();

    }


    void GihexWidget::set_position(i32 x, i32 y) {

        wgt_pos = {(f32) x * metrics_density, (f32) y * metrics_density};

        if (!container) return;
        calculate_position();
    }

    void GihexWidget::set_relative_to(GihexWidget *widget) {
        set_linear_to(widget);
        calculate_position();
    }

    void GihexWidget::set_linear_to(GihexWidget *widget) {
        widget_neighbor = widget;
    }

    void GihexWidget::calculate_position() {
        GxPos<i32> pos = container->calculate_child_pos(this, widget_neighbor);
        wgt_pos.x = (f32) pos.x;
        wgt_pos.y = (f32) pos.y;
    }


} // gihex