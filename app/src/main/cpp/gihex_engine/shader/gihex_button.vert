#version 300 es
precision mediump float;
in vec2 vertex_position;
in vec4 colors;

out vec4 color;
void main(){
    gl_Position.xy=(vertex_position,0.,1.);
//    gl_Position.z=0.;
    //    gl_Position.w=1.;
    color=colors;
}