//
// Created by gihexdev00 on 2/19/23.
//

#include <gihex_engine.h>

#include <android/window.h>

#include <paddleboat/paddleboat.h>

#include <utils/utils.h>
#include "widget/gihex_button.h"

namespace gihex {
    static GihexEngine *singleton_engine = nullptr;

    static GihexEngineSavedState app_state = {false};

    static bool all_motion_filter(const GameActivityMotionEvent *event) {
        // Process all motion events
        return true;
    }

    static void _handle_cmd_proxy(App *app, i32 cmd) {
        auto eng = (GihexEngine *) app->userData;
        eng->handle_cmd(cmd);
    }

    GihexEngine::GihexEngine(gihex::App *app) {
        GLOGI("Create Gihex Engine...");
        this->app = app;
        has_focus = is_visible = has_window = false;
        has_GL_object = false;
        egl_display = EGL_NO_DISPLAY;
        egl_surface = EGL_NO_SURFACE;
        egl_context = EGL_NO_CONTEXT;
        egl_config = nullptr; //0
        surface_size = {0, 0};
        android_api_version = 0;
        screen_density = AConfiguration_getDensity(app->config);
        active_axis_ids = 0;
        jni_env = nullptr;

        memset(&state, 0, sizeof(state));

        is_first_frame = true;

        app->motionEventFilter = all_motion_filter;

        if (app->savedState != nullptr) {
            // start with previous saved state
            state = *(GihexEngineSavedState *) app->savedState;
        }

        // G_ASSERT(singleton_engine==nullptr);
        // singleton_engine = this;
        auto a = Paddleboat_init(get_jni_env(), app->activity->javaGameActivity);
        app->onAppCmd = _handle_cmd_proxy;//=

        G_ASSERT(PADDLEBOAT_NO_ERROR == a);
        GLOGD("GihexEngine: querying API level.");
        GLOGI("GihexEngine: API version %d.", android_api_version);
        GLOGI("GihexEngine: Density %d", screen_density);
    }

    GihexEngine::~GihexEngine() {
        GLOGD("Destroy Gihex Engine...");
        Paddleboat_destroy(get_jni_env());
        kill_context();
        if (jni_env) {
            GLOGI("Detaching current thread from JNI.");
            app->activity->vm->DetachCurrentThread();
            GLOGI("Current thread detached from JNI.");
//            delete jni_env;
            jni_env = nullptr;
        }
        delete GihexSceneManager::get_instance();
        singleton_engine = nullptr;
        // do not delete singleton_engine internally
    }

    GihexEngine *GihexEngine::get_instance(gihex::App *app) {
        singleton_engine = (singleton_engine != nullptr) ? singleton_engine : new GihexEngine(app);
        return singleton_engine;
    }

    void GihexEngine::run_engine() {

        app->userData = this;

//        app->onAppCmd = _handle_cmd_proxy;
        // set activity window flags
        auto activity = GihexEngine::get_instance(app)->get_android_app()->activity;
        GameActivity_setWindowFlags(activity,
                                    AWINDOW_FLAG_KEEP_SCREEN_ON | AWINDOW_FLAG_TURN_SCREEN_ON |
                                    AWINDOW_FLAG_FULLSCREEN |
                                    AWINDOW_FLAG_SHOW_WHEN_LOCKED,
                                    0);
        update_system_bar_offset();
        while (true) {
            i32 events;
            PollSource *source;
            while (ALooper_pollAll(is_animating() ? 0 : -1, nullptr, &events, (void **) &source) >=
                   0) {
                if (source != nullptr) {
                    source->process(app, source);
                }
                if (app->destroyRequested) {
                    GLOGI("destroy_requested");
                    return;
                }
            }
            Paddleboat_update(get_jni_env());
            Paddleboat_setBackButtonConsumed(false);

            handle_activity_input();
            if (is_animating()) {
                render_frame();
            }
        }
    }

    void GihexEngine::handle_cmd(i32 cmd) {
        GLOGD("NativeEngine: handling command %d.", cmd);
        GihexSceneManager *sc_mgr = GihexSceneManager::get_instance();
        switch (cmd) {
            case APP_CMD_SAVE_STATE:
                GLOGI("APP_CMD_SAVE_STATE");
                state.has_focus = has_focus;
                app->savedState = malloc(sizeof(state));
                *((GihexEngineSavedState *) app->savedState) = state;
                app->savedStateSize = sizeof(state);
                sc_mgr->on_save_state();
                break;
            case APP_CMD_INIT_WINDOW:
                GLOGI("APP_CMD_INIT_WINDOW");

                if (nullptr == app->window) {
                    break;
                }
                has_window = true;
                sc_mgr->on_init_window();
                if (nullptr != app->savedState && sizeof(state) == app->savedStateSize) {
                    state = *((GihexEngineSavedState *) app->savedState);
                    has_focus = state.has_focus;
                    break;
                }
                // Workaround APP_CMD_GAINED_FOCUS issue where the focus state is not
                // passed down from NativeActivity when restarting Activity
                has_focus = app_state.has_focus;
                prepare_to_render();

                break;
            case APP_CMD_TERM_WINDOW:
                GLOGI("APP_CMD_TERM_WINDOW");

                kill_surface();
                has_window = false;
                sc_mgr->on_terminate_window();
                break;
            case APP_CMD_GAINED_FOCUS:
                GLOGI("APP_CMD_GAINED_FOCUS");

                state.has_focus = app_state.has_focus = has_focus = true;
                sc_mgr->on_gained_focus();
                break;
            case APP_CMD_LOST_FOCUS:
                GLOGI("APP_CMD_LOST_FOCUS");

                state.has_focus = app_state.has_focus = has_focus = false;
                sc_mgr->on_lost_focus();
                break;
            case APP_CMD_PAUSE:
                GLOGI("APP_CMD_PAUSE");

                sc_mgr->on_pause();
                break;
            case APP_CMD_RESUME:
                GLOGI("APP_CMD_RESUME");

                sc_mgr->on_resume();
                break;
            case APP_CMD_START:
                GLOGI("APP_CMD_START");
                //fix this make unexpected close when pause
                if (Paddleboat_isInitialized())
                    Paddleboat_onStart(get_jni_env());
                //                Paddleboat_setBackButtonConsumed(false);
                sc_mgr->on_start();
                is_visible = true;
                break;
            case APP_CMD_STOP:
                GLOGI("APP_CMD_STOP %p", jni_env);
                //fix this make unexpected close when pause
                if (Paddleboat_isInitialized())
                    Paddleboat_onStop(get_jni_env());
                is_visible = false;
                sc_mgr->on_stop();
                break;
            case APP_CMD_WINDOW_RESIZED:
                GLOGI("APP_CMD_WINDOW_RESIZED");
                sc_mgr->on_window_resized();
                break;
            case APP_CMD_CONFIG_CHANGED:
                GLOGI("APP_CMD_CONFIG_CHANGED");

                sc_mgr->on_config_changed();
                break;
            case APP_CMD_LOW_MEMORY:
                GLOGI("APP_CMD_LOW_MEMORY");

                if (has_window) {
                    kill_GL_object();
                }
                sc_mgr->on_low_memory();
                break;
            case APP_CMD_WINDOW_INSETS_CHANGED:
                GLOGI("APP_CMD_WINDOW_INSETS_CHANGED");

                update_system_bar_offset();
                sc_mgr->on_window_inset_changed();
                break;
            default:
                GLOGD("(unknown command).");
                break;
        }
    }

    void GihexEngine::update_system_bar_offset() {
        ARect inset;
        GameActivity_getWindowInsets(app->activity, GAMECOMMON_INSETS_TYPE_SYSTEM_BARS, &inset);
        system_bar_offset = inset.top;

    }

    void GihexEngine::render_frame() {
//        if (!prepare_to_render()) {
//            GLOGE("Failed to prepare render");
//            return;
//        }
        GihexSceneManager *scene_manager = GihexSceneManager::get_instance();



        // how big is the surface? We query every frame because it's cheap, and some
        // strange devices out there change the surface size without calling any callbacks...
        i32 w, h;
        eglQuerySurface(egl_display, egl_surface, EGL_WIDTH, &w);
        eglQuerySurface(egl_display, egl_surface, EGL_HEIGHT, &h);
        if (surface_size.w != w || surface_size.h != h) {
            surface_size.w = w;
            surface_size.h = h;

            scene_manager->set_scene_size(surface_size);
            glViewport(0, 0, surface_size.w, surface_size.h);
        }

        if (is_first_frame) {
            is_first_frame = false;
            fn_on_activate();
        }
//        render scene

//        scene_manager->update_scene();
        scene_manager->render_scene();


        //swap
        if (!eglSwapBuffers(egl_display, egl_surface)) {
            GLOGW("Failed swap buffer");
        }
    }

    void GihexEngine::handle_activity_input() {
        check_new_axis();
        android_input_buffer *input_buffer = android_app_swap_input_buffers(app);
        if (input_buffer == nullptr) { return; }
//        handle event
//use
//        int32_t Paddleboat_processInputEvent(const AInputEvent *event);

//or
//        int32_t Paddleboat_processGameActivityKeyInputEvent(const void *event,
//                                                            const size_t eventSize);
//        int32_t Paddleboat_processGameActivityMotionInputEvent(const void *event,
//                                                               const size_t eventSize);
//key event => keyboard, game controller, etc
        if (input_buffer->keyEventsCount != 0) {
            for (u64 i = 0; i < input_buffer->keyEventsCount; i++) {
                GameActivityKeyEvent *event = &input_buffer->keyEvents[i];
                if (!Paddleboat_processGameActivityKeyInputEvent(event,
                                                                 sizeof(GameActivityKeyEvent))) {
//                    if (event->keyCode == AKEYCODE_BACK) {
//                        if (event->action == AMOTION_EVENT_ACTION_UP)
//                            GLOGI("back button onclick");
//                        GameActivity_finish(app->activity);
//                    }
                    GihexSceneManager::get_instance()->key_event_handler(event);
                }
            }
            android_app_clear_key_events(input_buffer);
        }
//motion event => touchscreen, pointer, etc
        if (input_buffer->motionEventsCount != 0) {
            for (int i = 0; i < input_buffer->motionEventsCount; i++) {
                GameActivityMotionEvent *event = &input_buffer->motionEvents[i];
                if (!Paddleboat_processGameActivityMotionInputEvent(event,
                                                                    sizeof(GameActivityMotionEvent))) {
                    GihexSceneManager::get_instance()->motion_event_handler(event);
                }
            }
            android_app_clear_motion_events(input_buffer);
        }

    }

    void GihexEngine::check_new_axis() {
        u64 act_axis_ids = Paddleboat_getActiveAxisMask();
        u64 new_axis_ids = act_axis_ids ^ active_axis_ids;
        if (new_axis_ids) {
            active_axis_ids = act_axis_ids;
            i32 current_axis_id = 0;
            while (0 != new_axis_ids) {
                if ((new_axis_ids & 1) != 0) {
//                    enable axis
                    GameActivityPointerAxes_enableAxis(current_axis_id);
                }
                ++current_axis_id;
                new_axis_ids >>= 1;
            }
        }
    }

    bool GihexEngine::prepare_to_render() {
        if (!init_display()) {
            return false;
        }

        if (!init_surface()) {
            return false;
        }

        if (!init_context()) {
            return false;
        }

        if (!eglMakeCurrent(egl_display, egl_surface, egl_surface, egl_context)) {
            GLOGE("egl make current error %d", eglGetError());
        }
        if (!has_GL_object) {
            if (!init_GL_objects()) {
                return false;
            }
        }
        return true;
    }

    bool GihexEngine::init_display() {
        if (EGL_NO_DISPLAY != egl_display) return true;
        GLOGI("initializing display.");
        egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
        if (!eglInitialize(egl_display, 0, 0)) {
            GLOGE("failed init display...");
            return false;
        }
        return true;
    }

    bool GihexEngine::init_surface() {
        //need display
        G_ASSERT(EGL_NO_DISPLAY != egl_display)
        if (EGL_NO_SURFACE != egl_surface) {
            return true;
        }
        GLOGI("NativeEngine: initializing surface.");
        EGLint numb_configs;
        const EGLint attribs[] = {
                EGL_RENDERABLE_TYPE, EGL_OPENGL_ES3_BIT, // request OpenGL ES 3.0
                EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
                EGL_BLUE_SIZE, 8,
                EGL_GREEN_SIZE, 8,
                EGL_RED_SIZE, 8,
                EGL_DEPTH_SIZE, 16,
                EGL_NONE
        };
        // since this is a simple sample, we have a trivial selection process. We pick
        // the first EGLConfig that matches:
        eglChooseConfig(egl_display, attribs, &egl_config, 1, &numb_configs);
        // create EGL surface
        egl_surface = eglCreateWindowSurface(egl_display, egl_config, app->window, nullptr);
        if (EGL_NO_SURFACE == egl_surface) {
            GLOGE("Failed init surface");
            return false;
        }
        return true;
    }

    bool GihexEngine::init_context() {
        // need display
        G_ASSERT(EGL_NO_DISPLAY != egl_display)
        EGLint attribs[] = {EGL_CONTEXT_CLIENT_VERSION, 3, EGL_NONE};
        if (EGL_NO_CONTEXT != egl_context) {
            return true;
        }
        //create context
        GLOGI("NativeEngine: initializing context.");
        egl_context = eglCreateContext(egl_display, egl_config, nullptr, attribs);
        if (EGL_NO_CONTEXT == egl_context) {
            GLOGE("Failed init context");
            return false;
        }
        return true;
    }

    bool GihexEngine::init_GL_objects() {
        if (!has_GL_object) {
//            scene_manager->on_start_graphics();
            has_GL_object = true;
        }

        return true;
    }

    void GihexEngine::kill_surface() {
        eglMakeCurrent(egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (EGL_NO_SURFACE != egl_surface) {
            eglDestroySurface(egl_display, egl_surface);
            egl_surface = EGL_NO_SURFACE;
        }
        GLOGI("Surface killed successfully.");
    }

    void GihexEngine::kill_GL_object() {
        if (has_GL_object) {
//        scene_manager->kill_graphics();
            has_GL_object = false;
        }
    }

    void GihexEngine::kill_context() {
        kill_GL_object();
        eglMakeCurrent(egl_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        if (EGL_NO_CONTEXT != egl_context) {
            eglDestroyContext(egl_display, egl_context);
            egl_context = EGL_NO_CONTEXT;
        }
        GLOGI("Context killed successfully.");
    }

    JNIEnv *GihexEngine::get_jni_env() {
        if (!jni_env) {
            if (0 != app->activity->vm->AttachCurrentThread(&jni_env, nullptr)) {
                ABORT_GAME;
            }
            G_ASSERT(nullptr != jni_env);
        }
        return jni_env;
    }

    void GihexEngine::on_activate(const std::function<void()> &fn) {
        fn_on_activate = fn;
    }

    GihexSceneManager *GihexEngine::get_scene_manager() {
        return GihexSceneManager::get_instance();
    }

} // gihex