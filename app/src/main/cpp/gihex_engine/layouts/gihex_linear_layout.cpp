//
// Created by gihexdev00 on 3/13/23.
//

#include <layouts/gihex_linear_layout.h>

gihex::GihexLinearLayout::GihexLinearLayout(GxSize<u32> window_size, i32 density) : GihexLayout(
        window_size, density) {

}

gihex::GihexLinearLayout::~GihexLinearLayout() {

}

gihex::GxPos<i32>
gihex::GihexLinearLayout::calculate_child_pos(GihexWidget *widget, GihexWidget *widget_neighbor) {
    if (!widget_neighbor)
        widget->set_relative_to(widget_neighbor);
    GxPos<f32> pos = widget->get_position();
    if (get_numb_child() <= 1) {
        return {(i32) pos.x, (i32) pos.y};
    }

    if (orientation == LinearOrientation::VERTICAL) {
        return {(i32) pos.x,
                (i32) (widget_neighbor->get_square().bottom +
                       pos.y)
        };
    }
    return {(i32) (widget_neighbor->get_square().right +
                   pos.x), (i32) pos.y
    };
}

