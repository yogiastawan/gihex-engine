//
// Created by gihexdev00 on 3/13/23.
//

#include <layouts/gihex_relative_layout.h>

gihex::GihexRelativeLayout::GihexRelativeLayout(GxSize<u32> window_size, i32 density) : GihexLayout(
        window_size, density) {

}

gihex::GxPos<i32> gihex::GihexRelativeLayout::calculate_child_pos(gihex::GihexWidget *widget,
                                                             GihexWidget *widget_neighbor) {
    if (!widget->is_position_relative()) widget->set_relative(true);
    // this must set by user
//    if (!widget->get_widget_neighbor() && !widget_neighbor) {
//        widget->set_relative_to(widget_neighbor);
//    }
    GxPos<f32>pos = widget->get_position();
    GxPos<f32> new_pos = pos;
    if (widget_neighbor == nullptr) {
        // set position relative to root scene
        switch (widget->get_relative_position()) {
            case POS_RELATIVE_LEFT:
                new_pos.x = pos.x * metrics_density;
                break;
            case POS_RELATIVE_TOP:
                new_pos.y = pos.y * metrics_density;
                break;
            case POS_RELATIVE_RIGHT:
                new_pos.x = (f32) ((f32) win_size.w - pos.x) * metrics_density;
                break;
            case POS_RELATIVE_BOTTOM:
                new_pos.y = (f32) ((f32) win_size.h - pos.y) * metrics_density;
                break;
            case 3:
                new_pos.x = pos.x * metrics_density;
                new_pos.y = pos.y * metrics_density;
                break;
//                case 5:
//                    break;
            case 6:
                new_pos.y = pos.y * metrics_density;
                new_pos.x = (f32) ((f32) win_size.w - pos.x) * metrics_density;
                break;
            case 9:
                new_pos.x = pos.x * metrics_density;
                new_pos.y = (f32) ((f32) win_size.h - pos.y) * metrics_density;
                break;
//                case 10:
//                    break;
            case 12:
                new_pos.x = (f32) ((f32) win_size.w - pos.x) * metrics_density;
                new_pos.y = (f32) ((f32) win_size.h - pos.y) * metrics_density;
                break;
        }

        return {(i32) new_pos.x, (i32) new_pos.y};
    }

    //set position relative to widget
    switch (widget->get_relative_position()) {
        case POS_RELATIVE_LEFT:
            new_pos.x = widget_neighbor->get_square().left + (f32) pos.x;
            break;
        case POS_RELATIVE_TOP:
            new_pos.y = widget_neighbor->get_square().top + (f32) pos.y;
            break;
        case POS_RELATIVE_RIGHT:
            new_pos.x = widget_neighbor->get_square().right - (f32) pos.x;
            break;
        case POS_RELATIVE_BOTTOM:
            new_pos.y = widget_neighbor->get_square().bottom - (f32) pos.y;
            break;
        case 3:
            new_pos.x = widget_neighbor->get_square().left + (f32) pos.x;
            new_pos.y = widget_neighbor->get_square().top + (f32) pos.y;
            break;
//            case 5:
//                break;
        case 6:
            new_pos.y = widget_neighbor->get_square().top + (f32) pos.y;
            new_pos.x = widget_neighbor->get_square().right - (f32) pos.x;
            break;
        case 9:
            new_pos.x = widget_neighbor->get_square().left + (f32) pos.x;
            new_pos.y = widget_neighbor->get_square().bottom - (f32) pos.y;
            break;
//            case 10:
//                break;
        case 12:
            new_pos.x = widget_neighbor->get_square().right - (f32) pos.x;
            new_pos.y = widget_neighbor->get_square().bottom - (f32) pos.y;
            break;
    }
    return {(i32) new_pos.x, (i32) new_pos.y};

}

gihex::GihexRelativeLayout::~GihexRelativeLayout() {

}
