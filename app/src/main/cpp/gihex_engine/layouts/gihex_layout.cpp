//
// Created by gihexdev00 on 3/13/23.
//
#include <layouts/gihex_layout.h>

gihex::GihexLayout::GihexLayout(GxSize<u32> wn_size, i32 density)
        : GihexWidget(wn_size, density) {
    wgt_size = {(f32) win_size.w * metrics_density, (f32) win_size.h * metrics_density};


}

gihex::GihexLayout::~GihexLayout() {

}

void gihex::GihexLayout::key_event_handler(const GameActivityKeyEvent *event) {
    GihexWidget::key_event_handler(event);
}

void gihex::GihexLayout::motion_event_handler(const gihex::GXEvent &event) {
    GihexWidget::motion_event_handler(event);

    for (const std::shared_ptr<GihexWidget> &wd: child_widgets) {
        wd->motion_event_handler(event);
    }
}

void gihex::GihexLayout::update_widget() {
    GihexWidget::update_widget();
    for (const std::shared_ptr<GihexWidget> &wd: child_widgets) {
        wd->update_widget();
    }
}

void gihex::GihexLayout::render_widget() {
    GihexWidget::render_widget();
    for (const std::shared_ptr<GihexWidget> &wd: child_widgets) {
        wd->render_widget();
    }
}

bool gihex::GihexLayout::update_window_size_callback(gihex::GxSize<u32> new_win_size) {
    if (!GihexWidget::update_window_size_callback(new_win_size)) return false;
    for (const std::shared_ptr<GihexWidget> &wd: child_widgets) {
        wd->update_window_size_callback(new_win_size);
    }
    return true;
}

void gihex::GihexLayout::add_child(const std::shared_ptr<GihexWidget> &widget) {
    child_widgets.emplace_back(widget);
    GxPos<i32> pos = calculate_child_pos(widget.get(), child_widgets[child_widgets.size() - 1].get());
    widget->set_position(pos.x, pos.y);
    widget->set_container(this);
}
