//
// Created by gihexdev00 on 2/24/23.
//

#include <scene/gihex_scene_manager.h>

#include <scene/gihex_scene.h>
#include <utils/log.h>

namespace gihex {
    static GihexSceneManager *_singleton_sc_manager = nullptr;

    GihexSceneManager::GihexSceneManager() {
//        scenes.emplace_back(home_scene);
        GLOGD("Create Scene Manager obj...");
    }

    GihexSceneManager *
    GihexSceneManager::get_instance() {
        _singleton_sc_manager = (_singleton_sc_manager != nullptr) ? _singleton_sc_manager
                                                                   : new GihexSceneManager();
        return _singleton_sc_manager;
    }

    GihexSceneManager::~GihexSceneManager() {
        GLOGD("Destroy Scene Manager...");
        //important make sure this nullptr when destroyed
        _singleton_sc_manager = nullptr;
    }

    void GihexSceneManager::start_scene(const std::shared_ptr<GihexScene> &new_scene) {
        scenes.emplace_back(new_scene);
        current_scene = scenes.size() - 1;
    }


    void GihexSceneManager::replace_scene(const std::shared_ptr<GihexScene> &scene) {
        scenes[current_scene] = scene;
    }

    void GihexSceneManager::go_back() {
        if (current_scene <= 0) {
            // if only contains 1 scene don't do anything
            return;
        }
        current_scene -= 1;
    }

    void GihexSceneManager::on_start_graphics() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_start_graphics();
    }

    void GihexSceneManager::on_save_state() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_save_state();
    }

    void GihexSceneManager::on_init_window() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_init_window();
    }

    void GihexSceneManager::on_terminate_window() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_terminate_window();
    }

    void GihexSceneManager::on_gained_focus() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_gained_focus();
    }

    void GihexSceneManager::on_lost_focus() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_lost_focus();
    }

    void GihexSceneManager::on_pause() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_pause();
    }

    void GihexSceneManager::on_resume() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_resume();
    }

    void GihexSceneManager::on_stop() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_stop();
    }

    void GihexSceneManager::on_start() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_start();
    }

    void GihexSceneManager::on_window_resized() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_window_resized();
    }

    void GihexSceneManager::on_config_changed() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_config_changed();
    }

    void GihexSceneManager::on_low_memory() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_low_memory();
    }

    void GihexSceneManager::on_window_inset_changed() {
        if (scenes.empty()) return;
        scenes[current_scene]->on_window_inset_changed();
    }

    void GihexSceneManager::key_event_handler(const GameActivityKeyEvent *event) {
        if (scenes.empty()) return;
        scenes[current_scene]->key_event_handler(event);
    }

    void GihexSceneManager::motion_event_handler(const GameActivityMotionEvent *event) {
        if (scenes.empty()) return;
        scenes[current_scene]->motion_event_handler(event);
    }

    void GihexSceneManager::render_scene() {
        if (scenes.empty()) return;
        scenes[current_scene]->update_scene();
        scenes[current_scene]->render_scene();
    }

    void GihexSceneManager::set_scene_size(GxSize<u32> size) {
        if (scenes.empty()) return;
        scenes[current_scene]->set_scene_size(size);
    }

} // gihex