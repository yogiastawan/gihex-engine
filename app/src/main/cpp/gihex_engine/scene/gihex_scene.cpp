//
// Created by gihexdev00 on 2/24/23.
//

#include <scene/gihex_scene.h>

#include <paddleboat/paddleboat.h>

#include <utils/gihex_input.h>


namespace gihex {
    GihexScene::GihexScene(GihexEngine *engine) {
        eng = engine;
        scene_size = eng->get_win_size();
        GLOGD("Create Scene obj...");
    }

    GihexScene::~GihexScene() {
        GLOGD("Destroy Scene...");
    }

    void GihexScene::key_event_handler(const GameActivityKeyEvent *event) {
        for (const std::shared_ptr<GihexWidget> &widget: widgets) {
            widget->key_event_handler(event);
        }
    }

    void GihexScene::motion_event_handler(const GameActivityMotionEvent *event) {
//        GLOGD("Scene motion handler %d", event->source);
        GXEvent ev{};
        if (event->pointerCount > 0) {
            i32 action = event->action;
            i32 action_masked = action & AMOTION_EVENT_ACTION_MASK;
            i32 ptr_index = (action & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK)
                    >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
            if (ptr_index >= event->pointerCount) {
                return;
            }
            if (action_masked == AMOTION_EVENT_ACTION_DOWN ||
                action_masked == AMOTION_EVENT_ACTION_POINTER_DOWN) {
                ev.type = GxEventType::GX_EVENT_TYPE_POINTER_DOWN;
            } else if (action_masked == AMOTION_EVENT_ACTION_UP ||
                       action_masked == AMOTION_EVENT_ACTION_POINTER_UP) {
                ev.type = GxEventType::GX_EVENT_TYPE_POINTER_UP;
            } else if (action_masked == AMOTION_EVENT_ACTION_MOVE) {
                ev.type = GxEventType::GX_EVENT_TYPE_POINTER_MOVE;
            }
            ev.motion_pointer_id = event->pointers[ptr_index].id;
            ev.is_on_screen = event->source == AINPUT_SOURCE_TOUCHSCREEN;
            ev.motion_x = GameActivityPointerAxes_getX(&event->pointers[ptr_index]);
            ev.motion_y = GameActivityPointerAxes_getY(&event->pointers[ptr_index]);
        }
        for (const std::shared_ptr<GihexWidget> &widget: widgets) {
            widget->motion_event_handler(ev);
        }
    }

    void GihexScene::update_scene() {
        Paddleboat_update(eng->get_jni_env());
    }

    void GihexScene::render_scene() {
        egl_configure();
        for (const std::shared_ptr<GihexWidget> &wdg: widgets) {
            wdg->update_widget();
            wdg->render_widget();
        }
    }

    void GihexScene::on_save_state() {

    }

    void GihexScene::on_init_window() {

    }

    void GihexScene::on_terminate_window() {

    }

    void GihexScene::on_gained_focus() {

    }

    void GihexScene::on_lost_focus() {

    }

    void GihexScene::on_pause() {

    }

    void GihexScene::on_resume() {

    }

    void GihexScene::on_stop() {

    }

    void GihexScene::on_start() {

    }

    void GihexScene::on_window_resized() {

    }

    void GihexScene::on_config_changed() {

    }

    void GihexScene::on_low_memory() {

    }

    void GihexScene::on_window_inset_changed() {

    }

    void GihexScene::egl_configure() {
        glClearColor(1.0f, 0.0f, 1.0f, 1.0f);
        glEnable(GL_DEPTH_TEST);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    void GihexScene::on_start_graphics() {

    }

    void GihexScene::set_scene_size(gihex::GxSize<u32> size) {
        scene_size = size;
        for (const std::shared_ptr<GihexWidget> &wdg: widgets) {
            wdg->update_window_size_callback(size);
        }
    }


} // gihex