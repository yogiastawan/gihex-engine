//
// Created by gihexdev00 on 3/13/23.
//

#ifndef GIHEX_ENGINE_GIHEX_LINEAR_LAYOUT_H
#define GIHEX_ENGINE_GIHEX_LINEAR_LAYOUT_H

#include <layouts/gihex_layout.h>

namespace gihex {

    enum class LinearOrientation {
        VERTICAL,
        HORIZONTAL
    };

    class GihexLinearLayout : public GihexLayout {
    private:
        LinearOrientation orientation = LinearOrientation::VERTICAL;
    protected:
    public:
        GihexLinearLayout(GxSize<u32> winSize, i32 density);

        ~GihexLinearLayout();


        /**
         * Set Linear Orientation
         * @param orient LinearOrientation
         */
        inline void set_orientation(LinearOrientation orient) {
            orientation = orient;
        }

        GxPos<i32> calculate_child_pos(GihexWidget *widget,GihexWidget *widget_neighbor) override;
    };

}

#endif //GIHEX_ENGINE_GIHEX_LINEAR_LAYOUT_H
