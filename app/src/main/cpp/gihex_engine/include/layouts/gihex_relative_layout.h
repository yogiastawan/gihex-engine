//
// Created by gihexdev00 on 3/13/23.
//

#ifndef GIHEX_ENGINE_GIHEX_RELATIVE_LAYOUT_H
#define GIHEX_ENGINE_GIHEX_RELATIVE_LAYOUT_H

#include <layouts/gihex_layout.h>

namespace gihex {
    class GihexRelativeLayout : public GihexLayout {
    private:
    protected:
    public:
        GihexRelativeLayout(GxSize<u32> window_size, i32 density);

        ~GihexRelativeLayout();

        GxPos<i32> calculate_child_pos(GihexWidget *widget, GihexWidget *widget_neighbor) override;

    };
}

#endif //GIHEX_ENGINE_GIHEX_RELATIVE_LAYOUT_H
