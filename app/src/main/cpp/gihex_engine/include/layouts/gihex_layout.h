//
// Created by gihexdev00 on 3/13/23.
//

#ifndef GIHEX_ENGINE_GIHEX_LAYOUT_H
#define GIHEX_ENGINE_GIHEX_LAYOUT_H

#include <vector>
#include <memory>

#include <widget/gihex_widget.h>

namespace gihex {
//    class GihexWidget;

    class GihexLayout : public GihexWidget {
    private:
        std::vector<std::shared_ptr<GihexWidget>> child_widgets;

    protected:

    public:
        GihexLayout(GxSize<u32> win_size, i32 density);

        ~GihexLayout();

        void add_child(const std::shared_ptr<GihexWidget> &widget);

        void key_event_handler(const GameActivityKeyEvent *event) override;

        void motion_event_handler(const GXEvent &event) override;

        void update_widget() override;

        void render_widget() override;

        bool update_window_size_callback(GxSize<u32> new_win_size) override;

        size_t get_numb_child() const {
            return child_widgets.size();
        }

        std::shared_ptr<GihexWidget> get_last_child() const {
            return child_widgets[child_widgets.size() - 1];
        }

        virtual GxPos<i32> calculate_child_pos(GihexWidget *widget, GihexWidget *widget_neighbor) {
            GxPos<f32> pos = widget->get_position();
            return {(i32) pos.x, (i32) pos.y};
        }
    };
}

#endif //GIHEX_ENGINE_GIHEX_LAYOUT_H
