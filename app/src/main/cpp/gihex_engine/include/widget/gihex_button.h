//
// Created by gihexdev00 on 2/25/23.
//

#ifndef GIHEX_ENGINE_BUTTON_H
#define GIHEX_ENGINE_BUTTON_H

#include <functional>

#include <game-activity/GameActivity.h>

#include <widget/gihex_widget.h>

#include <utils/geometry.h>
#include <utils/mangle.h>
#include <utils/gihex_input.h>
#include <vector>

namespace gihex {

    class GihexButton : public GihexWidget {
    private:

        GxSquare padding = {8, 8, 8, 8};

        Color def_color = Color(0.2f, 0.2f, 0.2f, 1.0f);
        Color hover_color = Color(0.6f, 0.6f, 0.6f, 1.0f);

        std::function<void()> click;

        //opengl es attributes
        u32 vao = 0;
        u32 vbo[2] = {0, 0};
        u32 shader_program;

        std::vector<Color> bg_color{std::vector<Color>(6, def_color)};
        std::vector<f32> vertex_position{std::vector<f32>(12)};

        bool is_on_area(f32 ptr_x, f32 ptr_y) const;

        void change_vertex_pos();

    public:
        GihexButton(const GameActivity *activity, GxSize<u32> size_window, i32 density);

        ~GihexButton();

        void key_event_handler(const GameActivityKeyEvent *event);

        void motion_event_handler(const GXEvent &event);

        void update_widget();

        void render_widget();
        bool update_window_size_callback(GxSize<u32> new_win_size);

        void on_click(const std::function<void()> &click_handler);

        void set_position(i32 x, i32 y);

        void set_color(Color color);

        void set_hover_color(Color color);

        void set_visibility(bool visibility);

        void enable_widget(bool enable);
    };

} // gihex

#endif //GIHEX_ENGINE_BUTTON_H
