//
// Created by gihexdev00 on 2/24/23.
//

#ifndef GIHEX_ENGINE_WIDGET_H
#define GIHEX_ENGINE_WIDGET_H

#include <android/input.h>

#include <game-activity/GameActivity.h>

#include <utils/gihex_input.h>
#include <utils/android_utils.h>
#include <utils/geometry.h>


namespace gihex {
    class GihexLayout;

    enum PositionRelative : u8 {
        POS_RELATIVE_LEFT = 1,
        POS_RELATIVE_TOP = 2,
        POS_RELATIVE_RIGHT = 4,
        POS_RELATIVE_BOTTOM = 8
    };


    class GihexWidget {
    private:
        GxPos<f32> wgt_pos = {0, 0};
        PositionRelative relative_pos = static_cast<PositionRelative>(POS_RELATIVE_LEFT |
                                                                      POS_RELATIVE_TOP);

        bool is_pos_relative = false;

        void calculate_position();

    protected:
        GihexWidget *widget_neighbor = nullptr;
        GihexLayout *container = nullptr;

        bool is_visible = true;
        bool is_enable = true;
        GxSize<f32> wgt_size = {0, 0};
        f32 metrics_density = 0.75;
        GXEvent widget_event{};

        GxSize<u32> win_size = {400, 240};

        f32 x_gl_unit(f32 value) {
            return ((2.f * (f32) value) - (f32) win_size.w) / (f32) win_size.w;
        }

        f32 y_gl_unit(f32 value) {
            return ((f32) win_size.h - (2.0f * (f32) value)) / (f32) win_size.h;
        }


    public:
        GihexWidget(GxSize<u32> window_size, i32 density);

        ~GihexWidget();

        virtual void key_event_handler(const GameActivityKeyEvent *event);

        virtual void motion_event_handler(const GXEvent &event);

        virtual void update_widget();

        virtual void render_widget();

        virtual bool update_window_size_callback(GxSize<u32> new_win_size);

        virtual void set_position(i32 x, i32 y);

        GxSquareF get_square() const {
            return {wgt_pos.x, wgt_pos.y, wgt_pos.x + wgt_size.w, wgt_pos.y + wgt_size.h};
        }

        GxPos<f32> get_position() const {
            return wgt_pos;
        }

        void set_linear_to(GihexWidget *widget);

        void set_relative_to(GihexWidget *widget);

        void set_container(GihexLayout *root) {
            container = root;
        }

        void set_position_relative(PositionRelative pos);

        PositionRelative get_relative_position() const {
            return relative_pos;
        }

        GihexWidget *get_widget_neighbor() const {
            return widget_neighbor;
        }

        bool is_position_relative() {
            return is_pos_relative;
        }

        void set_relative(bool rel) {
            is_pos_relative = rel;
        }
    };

} // gihex

#endif //GIHEX_ENGINE_WIDGET_H
