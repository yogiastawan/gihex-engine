//
// Created by gihexdev00 on 3/3/23.
//

#ifndef GIHEX_ENGINE_SHADER_H
#define GIHEX_ENGINE_SHADER_H

#include <string>

#include <GLES3/gl3.h>

#include <utils/log.h>

namespace gihex {
    u32 create_shader_program(std::string vertex, std::string fragment) {
        //compile vertex
        u32 vt = glCreateShader(GL_VERTEX_SHADER);
        char const *vt_code = vertex.c_str();
        glShaderSource(vt, 1, &vt_code, nullptr);
        glCompileShader(vt);

        i32 state_compile;
        GLchar vertex_error[1024] = {0};
        glGetShaderiv(vt, GL_COMPILE_STATUS, &state_compile);
        if (state_compile == GL_FALSE) {
            i32 leng;
            glGetShaderInfoLog(vt, sizeof(vertex_error), &leng, vertex_error);
            GLOGD("Error compiling vertex shader: %s||%d", vertex_error, leng);
            return 0;
        }

        //compile fragment
        u32 ft = glCreateShader(GL_FRAGMENT_SHADER);
        char const *ft_code = fragment.c_str();
        glShaderSource(ft, 1, &ft_code, nullptr);
        glCompileShader(ft);
        glGetShaderiv(ft, GL_COMPILE_STATUS, &state_compile);
        if (state_compile == GL_FALSE) {
            glGetShaderInfoLog(ft, sizeof(vertex_error), nullptr, vertex_error);
            GLOGD("Error compiling fragment shader: %s", vertex_error);
            return 0;
        }

        u32 program = glCreateProgram();

        //link program
        glAttachShader(program, vt);
        glAttachShader(program, ft);
        glLinkProgram(program);
        i32 state_program;
        GLchar program_error[1024] = {0};
        glGetProgramiv(program, GL_LINK_STATUS, &state_program);
        if (state_program == GL_FALSE) {
            glGetProgramInfoLog(program, sizeof(program_error), nullptr, program_error);
            GLOGD("Error link program: %s", program_error);
            return 0;
        }

        // after linking program we do not need shader anymore;
        glDetachShader(program, vt);
        glDetachShader(program, ft);
        glDeleteShader(vt);
        glDeleteShader(ft);

        return program;
    }
}


#endif //GIHEX_ENGINE_SHADER_H
