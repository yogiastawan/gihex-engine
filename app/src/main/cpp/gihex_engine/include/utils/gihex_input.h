//
// Created by gihexdev00 on 3/3/23.
//

#ifndef GIHEX_ENGINE_INPUT_H
#define GIHEX_ENGINE_INPUT_H

#include <utils/mangle.h>

namespace gihex {

    enum class GxEventType {
        GX_EVENT_TYPE_POINTER_DOWN,
        GX_EVENT_TYPE_POINTER_UP,
        GX_EVENT_TYPE_POINTER_MOVE,
    };

    struct GXEvent {
        GxEventType type;
        i32 motion_pointer_id;
        bool is_on_screen;
        f32 motion_x;
        f32 motion_y;
    };

}
#endif //GIHEX_ENGINE_INPUT_H
