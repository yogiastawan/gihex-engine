//
// Created by gihexdev00 on 2/19/23.
//

#ifndef GIHEX_ENGINE_MANGLE_H
#define GIHEX_ENGINE_MANGLE_H

#include <cstdlib>

#include <android/input.h>
#include <game-activity/native_app_glue/android_native_app_glue.h>

#include <glm/glm.hpp>

typedef int8_t i8;
typedef uint8_t u8;
typedef int16_t i16;
typedef uint16_t u16;
typedef int32_t i32;
typedef uint32_t u32;
typedef int64_t i64;
typedef uint64_t u64;

typedef float f32;
typedef double f64;


namespace gihex {
    typedef struct android_app App;
    typedef struct android_poll_source PollSource;

    typedef struct CookEvent GCookEvent;

    typedef glm::vec4 Color;
}

#endif //GIHEX_ENGINE_MANGLE_H
