//
// Created by gihexdev00 on 2/19/23.
//

#ifndef GIHEX_ENGINE_GEOMETRY_H
#define GIHEX_ENGINE_GEOMETRY_H

#include "mangle.h"

namespace gihex {
    template<typename T>
    struct GxSize {
        T w;
        T h;
    };

    template<typename T>
    struct GxPos {
        T x;
        T y;
    };

    struct GxSquare {
        i32 left;
        i32 top;
        i32 right;
        i32 bottom;
    };

    struct GxSquareF {
        f32 left;
        f32 top;
        f32 right;
        f32 bottom;
    };

}
#endif //GIHEX_ENGINE_GEOMETRY_H
