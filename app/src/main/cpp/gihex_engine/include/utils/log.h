//
// Created by gihexdev00 on 2/19/23.
//

#ifndef GIHEX_ENGINE_LOG_H
#define GIHEX_ENGINE_LOG_H

#include <android/log.h>
#include <assert.h>

#define TAG_NAME_APP "GIHEX_ENGINE"

#define GLOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO,TAG_NAME_APP ,__VA_ARGS__))
#define GLOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN,TAG_NAME_APP,__VA_ARGS__))
#define GLOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR,TAG_NAME_APP,__VA_ARGS__))
#define GLOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG,TAG_NAME_APP,__VA_ARGS__))

#endif //GIHEX_ENGINE_LOG_H
