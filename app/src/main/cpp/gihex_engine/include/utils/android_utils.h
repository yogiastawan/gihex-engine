//
// Created by gihexdev00 on 3/8/23.
//

#ifndef GIHEX_ENGINE_ANDROID_UTILS_H
#define GIHEX_ENGINE_ANDROID_UTILS_H

#include <string>

#include <game-activity/GameActivity.h>

namespace gihex {
    class AndroidUtils {
    public:
        static std::string
        get_asset_string(const std::string& file_name, const std::string& path, const GameActivity *activity);
    };
}

#endif //GIHEX_ENGINE_ANDROID_UTILS_H
