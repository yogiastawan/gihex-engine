//
// Created by gihexdev00 on 2/23/23.
//

#ifndef GIHEX_ENGINE_UTILS_H
#define GIHEX_ENGINE_UTILS_H

#include <utils/log.h>

#define ABORT_GAME { GLOGE("*** GAME ABORTING."); *((volatile char*)0) = 'a'; }
#define DEBUG_BLIP GLOGI("[ BLIP ]: %s:%d", __FILE__, __LINE__)

#define G_ASSERT(cond) { if (!(cond)) { GLOGE("ASSERTION FAILED: %s", #cond); \
   ABORT_GAME; } }

#define BUFFER_OFFSET(i) ((char*)NULL + (i))
#endif //GIHEX_ENGINE_UTILS_H
