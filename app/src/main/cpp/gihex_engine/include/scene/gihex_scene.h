//
// Created by gihexdev00 on 2/24/23.
//

#ifndef GIHEX_ENGINE_SCENE_H
#define GIHEX_ENGINE_SCENE_H

#include <memory>
#include <vector>

#include <android/input.h>

#include <game-activity/GameActivity.h>

#include <gihex_engine.h>
#include <utils/geometry.h>
#include <widget/gihex_widget.h>

namespace gihex {
//    class GihexEngine;

    class GihexScene {
    private:


    protected:
        std::vector<std::shared_ptr<GihexWidget>> widgets;
        GihexEngine *eng;
        GxSize<u32> scene_size = {0, 0};

    public:
        GihexScene(GihexEngine *engine);

        ~GihexScene();

        // main scene function
        virtual void key_event_handler(const GameActivityKeyEvent *event);

        virtual void motion_event_handler(const GameActivityMotionEvent *event);

        virtual void update_scene();

        virtual void egl_configure();

        virtual void render_scene();

        virtual void on_start_graphics();

        void set_scene_size(gihex::GxSize<u32> size);

        void add_widget(const std::shared_ptr<GihexWidget> &widget) {
            widgets.emplace_back(widget);
        }

        // cycle activity cmd handler
        virtual void on_save_state();

        virtual void on_init_window();

        virtual void on_terminate_window();

        virtual void on_gained_focus();

        virtual void on_lost_focus();

        virtual void on_pause();

        virtual void on_resume();

        virtual void on_stop();

        virtual void on_start();

        virtual void on_window_resized();

        virtual void on_config_changed();

        virtual void on_low_memory();

        virtual void on_window_inset_changed();


    };

} // gihex

#endif //GIHEX_ENGINE_SCENE_H
