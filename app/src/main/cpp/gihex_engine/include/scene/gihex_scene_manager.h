//
// Created by gihexdev00 on 2/24/23.
//

#ifndef GIHEX_ENGINE_SCENE_MANAGER_H
#define GIHEX_ENGINE_SCENE_MANAGER_H

#include <vector>
#include <memory>

#include <android/input.h>

#include <utils/mangle.h>
#include <utils/geometry.h>
//#include <scene/gihex_scene.h>

namespace gihex {

    class GihexScene;

    class GihexSceneManager {
    private:
        std::vector<std::shared_ptr<GihexScene>> scenes;
        u8 current_scene = 0;

        GihexSceneManager();

    public:
        ~GihexSceneManager();

        static GihexSceneManager *get_instance();

        /**
         * Start new scene without deleting current scene.
         * @param new_scene shared pointer GihexScene to render
         */
        void start_scene(const std::shared_ptr<GihexScene> &new_scene);

        /**
         * Replace current scene
         * @param scene shared pointer GihexScene to render
         */
        void replace_scene(const std::shared_ptr<GihexScene> &scene);

        /**
        * Go to previous scene
        */
        void go_back();

        /**
        * Get current active scene
        * @return shared pointer of GihexScene
        */

        inline std::shared_ptr<GihexScene> get_current_scene() const {
            if (scenes.size() <= 0) return nullptr;
            return scenes[current_scene];
        }

        void key_event_handler(const GameActivityKeyEvent *event);

        void motion_event_handler(const GameActivityMotionEvent *event);

        void render_scene();

        void on_start_graphics();

//        life cycle cmd
        void on_save_state();

        void on_init_window();

        void on_terminate_window();

        void on_gained_focus();

        void on_lost_focus();

        void on_pause();

        void on_resume();

        void on_stop();

        void on_start();

        void on_window_resized();

        void on_config_changed();

        void on_low_memory();

        void on_window_inset_changed();

        void set_scene_size(GxSize<u32> size);
    };

} // gihex

#endif //GIHEX_ENGINE_SCENE_MANAGER_H
