//
// Created by gihexdev00 on 2/19/23.
//

#ifndef GIHEX_ENGINE_GIHEX_ENGINE_H
#define GIHEX_ENGINE_GIHEX_ENGINE_H

#include <memory>
#include <functional>

#include <game-activity/native_app_glue/android_native_app_glue.h>
#include <EGL/egl.h>
#include <GLES3/gl3.h>
#include <android/input.h>

#include <utils/mangle.h>
#include <utils/log.h>
#include <utils/geometry.h>
#include <scene/gihex_scene_manager.h>
//#include <scene/gihex_scene.h>

namespace gihex {

//    class GihexSceneManager;

//    class GihexScene;

    struct GihexEngineSavedState {
        bool has_focus;
    };

    class GihexEngine {
    private:
        GihexEngine(gihex::App *app);

        std::function<void()> fn_on_activate;

        // variables to track Android lifecycle:
        bool has_focus;
        bool is_visible;
        bool has_window;
        bool has_GL_object;

        i32 android_api_version;

        i32 screen_density;

        //EGL stuff
        EGLDisplay egl_display;
        EGLSurface egl_surface;
        EGLContext egl_context;
        EGLConfig egl_config;

        // system bar offset
        i32 system_bar_offset;

        // known surface size
        GxSize<u32> surface_size;
        //known native surface size
        GxSize<u32> native_surface_size;

        // known active motion axis ids (bitfield)
        u64 active_axis_ids;


        // android app struct
        App *app;

        // additional saved state
        GihexEngineSavedState state;

        // JNI environment
        JNIEnv *jni_env;

        //JNI env for the app native glue thread
        JNIEnv *app_jni_env;

        // is first frame
        bool is_first_frame;

        void kill_context();

        void update_system_bar_offset();

        inline bool is_animating() const {
            return has_focus && is_visible && has_window;
        }

        void render_frame();

        void handle_activity_input();

        void check_new_axis();


        bool prepare_to_render();

        bool init_display();

        bool init_surface();

        bool init_context();

        bool init_GL_objects();

        void kill_surface();

        void kill_GL_object();

    public:
        ~GihexEngine();

        static GihexEngine *get_instance(gihex::App *app);

        void run_engine();

        void handle_cmd(i32 cmd);
//        not used when using paddle bot controller
//        bool handle_event(AInputEvent *event);

        inline App *get_android_app() const {
            return app;
        }

//        inline GihexSceneManager *get_scene_manager() {
//            return GihexSceneManager::get_instance();
//        }

        inline gihex::GxSize<u32> get_win_size() {
            return surface_size;
        }

        JNIEnv *get_jni_env();

        void on_activate(const std::function<void()> &fn);

        GihexSceneManager *get_scene_manager();

        inline void exit_engine() {
            GameActivity_finish(GihexEngine::get_instance(app)->get_android_app()->activity);
        }

        inline i32 get_density() {
            return screen_density;
        }

    };

} // gihex

#endif //GIHEX_ENGINE_GIHEX_ENGINE_H
