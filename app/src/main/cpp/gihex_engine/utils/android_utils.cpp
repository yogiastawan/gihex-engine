//
// Created by gihexdev00 on 3/8/23.
//
#include <utils/android_utils.h>

namespace gihex {

    std::string AndroidUtils::get_asset_string(const std::string& file_name, const std::string& path,
                                               const GameActivity *activity) {
        AAssetManager *asset_manager = activity->assetManager;
        AAssetDir *dir = AAssetManager_openDir(asset_manager, path.c_str());
        while (AAssetDir_getNextFileName(dir) != nullptr) {
            AAsset *asset = AAssetManager_open(asset_manager, file_name.c_str(),
                                               AASSET_MODE_BUFFER);
            size_t file_length = AAsset_getLength(asset);
            char *content = new char[file_length + 1];
            AAsset_read(asset, content, file_length);
            content[file_length] = '\0';
            std::string f_content = std::string(content);
            return f_content;
        }
        return "";
    }
}