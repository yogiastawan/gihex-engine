//
// Created by gihexdev00 on 3/2/23.
//

#include "home_scene.h"

#include <memory>

#include <jni.h>

#include <widget/gihex_button.h>

HomeScene::HomeScene(gihex::GihexEngine *engine) : GihexScene(engine) {
    create_ui();
}

HomeScene::~HomeScene() = default;


void HomeScene::update_scene() {
//    override update scene here
}

void HomeScene::key_event_handler(const GameActivityKeyEvent *event) {
    GihexScene::key_event_handler(event);
    if (event->keyCode == AKEYCODE_BACK) {
        if (event->action == AMOTION_EVENT_ACTION_UP)
            GLOGI("back button onclick");
//        GameActivity_finish(eng->get_android_app()->activity);
        eng->exit_engine();
//        jmethodID finish = eng->get_jni_env()->GetMethodID(
//                eng->get_jni_env()->GetObjectClass(eng->get_android_app()->activity->javaGameActivity), "gameFinish",
//                "()V");
//        eng->get_jni_env()->CallVoidMethod(eng->get_android_app()->activity->javaGameActivity,
//                                           finish);
    }

}

void HomeScene::create_ui() {

    auto btn = std::make_shared<gihex::GihexButton>(eng->get_android_app()->activity, scene_size,
                                                    eng->get_density());
    btn->set_position(scene_size.w - 80, scene_size.h -50/ 2);
    btn->on_click([]() {
        GLOGI("btn clicked");
    });

    widgets.emplace_back(btn);

    auto btn2 = std::make_shared<gihex::GihexButton>(
            gihex::GihexButton(eng->get_android_app()->activity, scene_size, eng->get_density()));
    btn2->set_position(scene_size.w-80 / 2, scene_size.h-50 / 2);
    btn2->on_click([=]() {
        GLOGI("btn 2 clicked");
//        eng->exit_engine();
    });
    widgets.emplace_back(btn2);
    GLOGD("scene size: %dx%d", scene_size.w, scene_size.h);

    auto btn3 = std::make_shared<gihex::GihexButton>(
            gihex::GihexButton(eng->get_android_app()->activity, scene_size, eng->get_density()));
    btn3->set_position(0, 0);
    btn3->on_click([]() {
        GLOGI("btn 3 clicked");
    });
    widgets.emplace_back(btn3);


}
