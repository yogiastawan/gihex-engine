#include <memory>

#include <gihex_engine.h>

#include "home_scene.h"

extern "C" {
void android_main(gihex::App *app);
}

void android_main(gihex::App *app) {
    gihex::GihexEngine *engine = gihex::GihexEngine::get_instance(app);
    engine->on_activate([engine] {
        auto home_scene = std::make_shared<HomeScene>(HomeScene(engine));
        engine->get_scene_manager()->start_scene(home_scene);
        GLOGD("add scene");
    });

    engine->run_engine();
    delete engine;
}