//
// Created by gihexdev00 on 3/2/23.
//

#ifndef GIHEX_ENGINE_HOME_SCENE_H
#define GIHEX_ENGINE_HOME_SCENE_H

#include <scene/gihex_scene.h>

class HomeScene : public gihex::GihexScene {
private:
public:
    HomeScene(gihex::GihexEngine *engine);

    ~HomeScene();

    void update_scene();

    void key_event_handler(const GameActivityKeyEvent *event);

    void create_ui();
};


#endif //GIHEX_ENGINE_HOME_SCENE_H
